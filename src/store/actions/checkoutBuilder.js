import {ADD_DISH, INIT_DISH, REMOVE_DISH} from "./actionTypes";

export const addDish = (dishName, price) => ({type: ADD_DISH, dishName, price});

export const removeDish = (dishName, price ) => ({type: REMOVE_DISH, dishName, price});

export const initDishes = () => ({type: INIT_DISH});