import axios from '../../axios-orders';

import {
    ORDER_FAILURE,
    ORDER_REQUEST,
    ORDER_SUCCESS} from "./actionTypes";
import {initDishes} from "./checkoutBuilder";

export const orderRequest = () => ({type: ORDER_REQUEST});
export const orderSuccess = dishes => ({type: ORDER_SUCCESS, dishes});
export const orderFailure = error => ({type: ORDER_FAILURE, error});

export const createMenu = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('dishes.json').then(response => {
                dispatch(orderSuccess(response.data));
            },
            error => dispatch(orderFailure(error))
        );
    }
};
export const createOrder = (orderData, history) => {
    return dispatch => {
        dispatch(orderRequest());

        axios.post('orders.json', orderData).then(
            response => {
                console.log(response, 'RESPONSE');
                dispatch(createMenu());
                dispatch(initDishes());
                history.push('/');
            },
            error => dispatch(orderFailure(error))
        );
    }
};
