import {ADD_DISH, INIT_DISH, REMOVE_DISH} from "../actions/actionTypes";



const INITIAL_INGREDIENTS = {
    burger: 0,
    cheeseburger: 0,
    cola: 0,
    fries: 0,
    shaurma: 0,
    tea: 0,
};

const INITIAL_PRICE = 150;

const initialState = {
    ingredients: {...INITIAL_INGREDIENTS},
    totalPrice: INITIAL_PRICE,
    initPrice: INITIAL_PRICE,
};
const checkoutBuilderReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.dishName]: state.ingredients[action.dishName] + 1
                },
                totalPrice: state.totalPrice + action.price
            };
        case  REMOVE_DISH:
            if (state.ingredients[action.dishName] > 0) {
                return {
                    ...state,
                    ingredients: {
                        ...state.ingredients,
                        [action.dishName]: state.ingredients[action.dishName] - 1
                    },
                    totalPrice: state.totalPrice - action.price
                };
            } else {
                return state;
            }
        case INIT_DISH:
            return {
                ...state,
                ingredients: {...INITIAL_INGREDIENTS},
                totalPrice: INITIAL_PRICE
            };
        default:
            return state;
    }
};

export default checkoutBuilderReducer;