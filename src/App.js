import React, { Component } from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import DishList from "./containers/DishList/DishList";


class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={DishList} />
                    {/*<Route path="/checkout" component={Checkout} />*/}
                    {/*<Route path="/orders" component={Orders} />*/}
                    <Route render={() => <h1>No found!</h1>} />
                </Switch>
            </Layout>
        );
    }
}

export default App;

