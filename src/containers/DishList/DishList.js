import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createMenu, createOrder} from "../../store/actions/order";
import Spinner from "../../components/UI/Spinner/Spinner";

import './DishList.css';
import Button from "../../components/UI/Button/Button";
import {addDish, removeDish} from "../../store/actions/checkoutBuilder";
import Modal from "../../components/UI/Modal/Modal";


class DishList extends Component {
    state = {
        purchasing: false,
        name: '',
        email: '',
        street: '',
        postal: '',
    };

    componentDidMount(){
        this.props.createMenu()
    }

    isPurchasable = () => {
        const sum = Object.values(this.props.ingredients)
            .reduce((sum, el) => sum + el, 0);

        return sum > 0;
    };

    purchase = () => {
        this.setState({purchasing: true});
    };

    purchaseCancel = () => {
        this.setState({purchasing: false});
    };

    purchaseContinue = () => {
        // this.props.initOrder();
        this.setState({purchasing: false});
        this.props.history.push('/');
    };

    orderHandler = event => {
        event.preventDefault();

        const orderData = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {...this.state}
        };

        this.props.createOrder(orderData, this.props.history);
    };

    valueChanged = event => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    render() {
        console.log(this.props.dishes, 'DISHES');
        return (
            <Fragment>
                <div className='wrap'>
                    {this.props.loading ? <Spinner/> : Object.keys(this.props.dishes).map((order, key) => {
                        const oneOrder = this.props.dishes[order];
                        return <div key={key} className='wrapper'>
                            <img className='img' src={oneOrder.pictureDishes} alt="img"/>
                            <span className='title'>{oneOrder.title}</span>
                            <span className='price'>{oneOrder.price}KGS</span>
                            <Button onClick={() => this.props.addDish(order, oneOrder.price)} btnType='Success' className='btn'>Add to card</Button>
                        </div>
                    })}

                    <div className='card'>

                        {Object.keys(this.props.ingredients).map((order, key) => {
                            const nameOrder = this.props.ingredients[order];
                            if (nameOrder > 0){return<p className='cardItem' key={key}>
                                    {this.props.dishes[order].title}
                                    <span>X</span>
                                    {nameOrder}
                                    <span>{this.props.dishes[order].price*nameOrder}KGS</span>

                                    <Button
                                        onClick={() => this.props.removeDish(order, this.props.dishes[order].price)}
                                        btnType='Danger'
                                        className='btn'>
                                        Remove one position
                                    </Button>

                                </p>}
                        })}
                        <div className='total'>
                            <p>Доставка{this.props.initPrice}</p>
                            <p>Итого:{this.props.totalPrice ===150 ? null:this.props.totalPrice}</p>
                            {this.props.totalPrice === 150 ? null :<Button
                                    btnType='Get'
                                    onClick={this.purchase}
                                >
                                    Place order
                                </Button>}

                            <Modal
                                show={this.state.purchasing}
                                close={this.purchaseCancel}

                            >
                                <form onSubmit={this.orderHandler}>
                                    <input className="Input" type="text" name="name" placeholder="Your Name"
                                           value={this.state.name} onChange={this.valueChanged}
                                    />
                                    <input className="Input" type="email" name="email" placeholder="Your Mail"
                                           value={this.state.email} onChange={this.valueChanged}
                                    />
                                    <input className="Input" type="text" name="street" placeholder="Street"
                                           value={this.state.street} onChange={this.valueChanged}
                                    />
                                    <input className="Input" type="text" name="postal" placeholder="Postal Code"
                                           value={this.state.postal} onChange={this.valueChanged}
                                    />
                                    <Button onClick={()=>this.purchaseContinue()} btnType="BtnOrder">ORDER</Button>

                                </form>
                            </Modal>
                        </div>


                    </div>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    dishes: state.or.dishes,
    totalPrice: state.bb.totalPrice,
    loading: state.or.loading,
    ingredients: state.bb.ingredients,
    initPrice: state.bb.initPrice,
});

const mapDispatchToProps = dispatch => ({
    createMenu: () => dispatch(createMenu()),
    createOrder: (orderData, history) => dispatch(createOrder(orderData,history)),
    addDish: (name, price) => dispatch(addDish(name, price)),
    removeDish: (name, price) => dispatch(removeDish(name, price))


});

export default connect(mapStateToProps, mapDispatchToProps) (DishList);